<?php namespace Roodev\Rooblog;

use Illuminate\Database\Seeder;
use DB;
use Eloquent;

class DatabaseSeeder extends Seeder {

    public function run()
    {
        Eloquent::unguard();

        $this->call(__NAMESPACE__ . '\\BlogSeeder');
        $this->call(__NAMESPACE__ . '\\OptionSeeder');
        $this->call(__NAMESPACE__ . '\\CategorySeeder');
        $this->call(__NAMESPACE__ . '\\PostSeeder');
    }

}
<?php namespace Roodev\Rooblog;

use Illuminate\Database\Seeder;
use DB;

class PostSeeder extends Seeder {

    public function run()
    {
        DB::table('roo_post')->delete();

        $posts  = array(
            array(
                'title'         => 'Asyik... BBM-an Kini Juga Bisa Lewat PC',
                'content'       => '<p>London - Kesan ekslusif BlackBerry perlahan mulai hilang. Sejak dibuka lintas platform ke iOS, Android, dan terakhir Windows Phone, kini pengguna bisa mengakses BlackBerry Messenger (BBM) dari platform mana saja, termasuk lewat PC.</p>

<p>Ekslusivitas itu pun akhirnya luntur sejak &#8206;BlackBerry memperkenalkan aplikasi bernama BlackBerry Blend. Dengan fitur ini, semua hal yang tadinya cuma bisa dilakukan di BlackBerry kini bisa dilakukan di mana saja melalui berbagai perangkat.</p>

<p>Dalam demo yang disaksikan langsung oleh detikINET di London, Inggris, Rabu (24/9/2014), dengan fitur BlackBerry Blend ini kita juga bisa chatting dengan BBM melalui PC desktop.</p>

<p>Bukan itu saja, dengan BlackBerry Blend, kita juga bisa mengakses email, SMS, dan bahkan agenda kalender. Sehingga berbagi informasi sudah bisa dilakukan tanpa mesti terhubung langsung dengan perangkat BlackBerry.</p>

<p>Aplikasi ini berjalan di PC, baik laptop maupun desktop, serta tablet, dengan sistem operasi Windows, Mac, iOS dan Android. Dan bisa digunakan bagi pengguna BlackBerry dengan sistem Operasi BlackBerry 10.</p>',
                'created_at'    => date('Y-m-d H:i:s'),
                'updated_at'    => date('Y-m-d H:i:s'),
            ),
            array(
                'title'         => 'Tantang iPhone 6, Samsung Percepat Pemasaran Note 4',
                'content'       => '<p>Jakarta - Penjualan iPhone 6 dan iPhone 6 Plus yang dilaporkan telah mencapai 10 juta unit membuat rival waspada. Samsung pun melakukan langkah antisipasi dengan mempercepat waktu pemasaran Galaxy Note 4, phablet terbarunya.</p>

<p>&quot;Reaksi positif dari konsumen terhadap dua perangkat Apple itu membuat kami mempercepat pemasaran Note 4 dari jadwal sebelumnya. Samsung akan agresif mempromosikannya,&quot; kata sumber eksekutif Samsung, seperti dikutip detikINET dari Korea Times, Rabu (24/9/2014).</p>

<p>Pemasaran di Korea Selatan dipercepat menjadi 26 September dari sebelumnya dijadwalkan bulan Oktober. Sepertinya, pemasaran di negara-negara lain pun akan dimajukan dari rencana semula.</p>

<p>Samsung mencanangkan ambisi besar dalam penjualan Note 4. Tahun lalu, Note 3 terjual 10 juta unit dalam dua bulan. Kini targetnya lebih membumbung.</p>

<p>&quot;Samsung ingin mengapalkan 15 juta Note 4 dalam 30 hari sesudah produk itu dipasarkan, yang memang sangat ambisius,&quot; kata pejabat Samsung lain yang dikutip Korea Times.</p>',
                'created_at'    => date('Y-m-d H:i:s'),
                'updated_at'    => date('Y-m-d H:i:s'),
            ),
            array(
                'title'         => 'Begini Cara Perbaiki Baterai Boros & WiFi Lelet di iOS 8',
                'content'       => '<p>Jakarta - Beberapa pengguna iDevice yang sudah update ke iOS 8 mengeluhkan masalah lambatnya saat berada di jaringan WiFi dan baterai yang cepat boros. Tak perlu menunggu update dari Apple, Anda pun bisa memperbaikinya detik ini juga.</p>

<p>Seperti diberitakan sebelumnya, begitu iOS 8 terinstal dan iPhone digunakan, baterai terkuras dalam waktu singkat.</p>

<p>&quot;Bateraiku habis begitu cepat. Dari 100% menjadi kosong hanya sekitar 4 jam dengan penggunaan minimal. Hal ini terjadi tepat sesudah upgrade ke iOS 8 baik di iPhone 5S maupun iPad Air,&quot; tulis salah seorang pemakai iPhone, melalui forum Apple.</p>

<p>detikINET pun mengalami masalah sama. Dari hasil pengalaman menggunakan iPhone 5S, baterai kini 30% lebih cepat habis dari biasanya. Padahal, tidak banyak aktivitas yang dilakukan di perangkat yang detikINET gunakan.</p>

<p>Sementara itu, pengguna iOS 8 yang berada di jaringan WiFi malah menjadi lambat dalam mengakses ke internet.</p>

<p>Dari pengalaman, beberapa WiFi yang dijajal detikINET nyaris tak bisa membuka browser atau lambat mengirimkan email. Padahal, bila dijajal dengan iDevice yang belum update, hasilnya normal.</p>',
                'created_at'    => date('Y-m-d H:i:s'),
                'updated_at'    => date('Y-m-d H:i:s'),
            ),
        );

        $category   = Models\Category::where('slug', '=', 'default')->first();

        foreach ($posts as $post) {
            $mpost              = new Models\Post;
            $mpost->title       = $post['title'];
            $mpost->content     = $post['content'];
            $mpost->created_at  = $post['created_at'];
            $mpost->updated_at  = $post['updated_at'];
            $mpost->save();

            $mpost->category()->attach($category->id);
        }
    }

}

<?php namespace Roodev\Rooblog;

use Illuminate\Database\Seeder;
use DB;

class CategorySeeder extends Seeder {

    public function run()
    {
        DB::table('roo_category')->delete();

        $category   = array(
            'parent_id' => 0,
            'name'      => 'Default',
            'slug'      => 'default',
            'removable' => false,
        );

        Models\Category::create($category);
    }

}
<?php namespace Roodev\Rooblog;

use Illuminate\Database\Seeder;
use DB;

class BlogSeeder extends Seeder {

    public function run()
    {
        DB::table('roo_blog')->delete();

        $blog   = array(
            'name'          => 'Rooblog Web',
            'url'           => url(),
            'slogan'        => 'Rooblog Berhati Nyaman',
            'description'   => 'Rooblog merupakan blog simple dari Roodev',
            'status'        => 'active',
            'created_at'    => date('Y-m-d H:i:s'),
            'updated_at'    => date('Y-m-d H:i:s'),
        );

        Models\Blog::create($blog);
    }

}
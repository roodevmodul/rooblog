<?php namespace Roodev\Rooblog;

trait RoopageTrait {

    /**
     * Inisialisasi Model Page, defaultnya menggunakan Model Post
     * 
     * @return Models\Post
     */
    public function page()
    {
        return $this->model->post;
    }

    /**
     * Menambahkan data page ke database
     * 
     * @param  array $data Data page yang akan disimpan, minimal title dan content
     * 
     * @return Models\Post|boolean
     */
    public function storePage(array $data)
    {
        if ($this->page()->validate($data)) {
            return $this->storePageForce($data);
        } else {
            $this->setErrors($this->page()->errors());
            
            return false;
        }
    }

    /**
     * Menambahkan data page ke database tanpa validasi
     * 
     * @param  array $data Data page yang akan disimpan, minimal title dan content
     * 
     * @return Models\Post
     */
    public function storePageForce(array $data)
    {
        $this->page()->type   = 'page';

        foreach ($data as $key => $value)
            $this->page()->{$key} = $value;
            $this->page()->save();

            return $this->page();
    }

    /**
     * Mengambil banyak data page dari database
     * dapat diambil dengan batasan untuk pagination
     * 
     * @param  integer $perPage
     * 
     * @return Illuminate\Database\Eloquent\Collection
     */
    public function showPages($perPage = 0)
    {
        return $this->page()->pageOnly()->descending()->paginate($perPage);
    }

    /**
     * Mengambil banyak data page dengan limit
     * nilai defaultnya adalah 20   
     *
     * @param  integer $limit Limit banyak data yang akan diambil
     *
     * @return Illuminate\Database\Eloquent\Collection
     */
    public function showPagesWithLimit($limit = 20)
    {
        return $this->page()->pageOnly()->descending()->limit($limit)->get();
    }

    /**
     * Mengambil sebuah data page berdasarkan id (primary key)
     *
     * @param  integer $id Id dari sebuah data page yang akan diambil
     *
     * @return Models\Post
     */
    public function showPage($id)
    {
        return $this->page()->pageOnly()->find($id);
    }

    /**
     * Mengambil sebuah data page berdasarkan slug
     *
     * @param  string $slug Slug dari sebuah page yang akan diambil
     *
     * @return Models\Post 
     */
    public function showPageBySlug($slug)
    {
        return $this->page()->slug($slug);
    }

    /**
     * Memperbaharui sebuah data page berdasarkan id dengan validasi
     *
     * @param  integer $id Id dari data page yang akan diperbaharui
     * @param  array $data Data yang baru, minimal title dan content
     * @param  boolean $updateSlug Apakah slug juga akan diperbaharui
     *
     * @return Models\Post|boolean
     */
    public function updatePage($id, array $data, $updateSlug = false)
    {
        if ($this->page()->validate($data)) {
            return $this->updatePageForce($id, $data, $updateSlug);
        } else {
            $this->setErrors($this->page()->error());
            
            return false;
        }
    }

    /**
     * Memperbaharui sebuah data page berdasarkan id
     *
     * @param  integer $id Id dari sebuah data page yang akan diperbaharui
     * @param  array $data Data page yang baru
     * @param  boolean $updateSlug Apakah slug juga diperbaharui?
     *
     * @return Models\Post
     */
    public function updatePageForce($id, array $data, $updateSlug = false)
    {
        $page   = $this->showPage($id);

        foreach ($data as $key => $value)
            $page->{$key} = $value;

        if ($updateSlug)
            $page->resluggify();

        $page->save();

        return $page;
    }

    /**
     * Menghapus sebuah data page berdasarkan id (primary key)
     *
     * @param  integer $id Id data page yang akan dihapus
     *
     * @return boolean
     */
    public function destroyPage($id)
    {
        if($page = $this->showPage($id)) {
            return $page->delete();
        } else {
            return false;
        }
    }

}
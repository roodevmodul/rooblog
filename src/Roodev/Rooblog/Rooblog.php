<?php namespace Roodev\Rooblog;

use Config;

/**
 * Library untuk melakukan pengelolaan blog dengan mudah
 * 
 * @version  1.0.0-beta
 * 
 * @author  Roodev Team
 * @author  Habib Nurrahman <nurmanhabib@gmail.com>
 * 
 */
class Rooblog {

    use RoopostTrait;
    use RoopageTrait;

    /**
     * Object model yang sedang aktif digunakan.
     *
     * @var Models\Model
     */
    public $model;

    /**
     * Pesan error dari aktifitas yang sedang aktif.
     *
     * @var string
     */
    public $errors;

    /**
     * Inisialisasi attribute model dan konfigurasi
     */
    public function __construct()
    {
        $this->model    = new Models\Model;
        $this->config   = Config::get('rooblog::model');
    }

    /**
     * Membuat instansiasi dari Class Rooblog
     * 
     * @return Rooblog
     */
    public function getInstance()
    {
        return new Rooblog;
    }

    /**
     * Mengambil informasi blog
     *
     * @return string
     */
    public function info()
    {
        return $this->model->blog->first();
    }

    /**
     * Set pesan error untuk siap ditampilkan
     *
     * @param mixed $errors Pesan yang dikirimkan
     */
    
    public function setErrors($errors)
    {
        $this->errors = $errors;

        return $this;
    }

    /**
     * Mengambil pesan errors
     * 
     * @return mixed
     */
    public function errors()
    {
        return $this->errors;
    }

    public function __toString()
    {
        return (string) $this->info();
    }

}
<?php namespace Roodev\Rooblog\Exception;

use Exception;
use Response;

class ValidatorException extends Exception {

    public function __construct($message, $code, Exception $previous = null)
    {
        // Keep message to instanceof Illuminate\Support\MessageBag
        $this->message  = $message;
        $this->code     = $code;
        $this->previous = $previous;
    }

    public function errorMessage()
    {
        $message    = $this->message;
        $result     = array();

        foreach ($message as $m)
            $result[]   = $m;

        return $message;
    }

}
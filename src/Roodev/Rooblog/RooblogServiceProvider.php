<?php namespace Roodev\Rooblog;

use Illuminate\Support\ServiceProvider;

class RooblogServiceProvider extends ServiceProvider {

	/**
	 * Indicates if loading of the provider is deferred.
	 *
	 * @var bool
	 */
	protected $defer = false;

	/**
	 * Bootstrap the application events.
	 *
	 * @return void
	 */
	public function boot()
	{
		$this->package('roodev/rooblog');
	}

	/**
	 * Register the service provider.
	 *
	 * @return void
	 */
	public function register()
	{
		$this->app->register('Cviebrock\EloquentSluggable\SluggableServiceProvider');

		$this->app->bind('rooblog', function()
		{
			return new Rooblog;
		});

		$this->app->booting(function()
		{
			$loader	= \Illuminate\Foundation\AliasLoader::getInstance();
			$loader->alias('Rooblog', 'Roodev\Rooblog\Facades\Rooblog');
		});
	}

	/**
	 * Get the services provided by the provider.
	 *
	 * @return array
	 */
	public function provides()
	{
        return array();
	}

}

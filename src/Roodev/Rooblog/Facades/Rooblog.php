<?php namespace Roodev\Rooblog\Facades;

use Illuminate\Support\Facades\Facade;

class Rooblog extends Facade {

    protected static function getFacadeAccessor() { return 'rooblog'; }

}
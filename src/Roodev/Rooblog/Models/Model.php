<?php namespace Roodev\Rooblog\Models;

class Model {

    public $post;
    public $blog;
    public $category;
    public $option;

    public function __construct()
    {
        $this->post     = new Post;
        $this->blog     = new Blog;
        $this->option   = new Option;
    }

}
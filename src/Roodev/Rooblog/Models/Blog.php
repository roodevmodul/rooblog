<?php namespace Roodev\Rooblog\Models;

class Blog extends Eloquent {

    protected $table    = 'roo_blog';

    public function category()
    {
        return $this->hasMany(__NAMESPACE__ . '\\Category', 'roo_category');
    }

}
<?php namespace Roodev\Rooblog\Models;

class Option extends Eloquent {

    protected $table    = 'roo_options';

    public $timestamps  = false;

    public function blog()
    {
        return $this->belongsTo('Blog');
    }

}
<?php namespace Roodev\Rooblog\Models;

class Category extends Eloquent {

    protected $table    = 'roo_category';

    public $timestamps  = false;

    public function post()
    {
        return $this->belongsToMany(__NAMESPACE__ . '\\Post', 'roo_post_category', 'category_id', 'post_id');
    }

}
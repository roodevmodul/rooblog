<?php namespace Roodev\Rooblog\Models;

use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;
use Str;

class Post extends Eloquent implements SluggableInterface {

    use SluggableTrait;

    protected $table        = 'roo_post';

    protected $rules        = [
        'title'     => 'required|min:3',
        'content'   => 'required',
    ];

    protected $sluggable    = array(
        'build_from' => 'title',
        'save_to'    => 'slug',
    );

    public function category()
    {
        return $this->belongsToMany(__NAMESPACE__ . '\\Category', 'roo_post_category');
    }

    public function scopeSlug($query, $slug)
    {
        return $query->where('slug', '=', $slug)->first();
    }

    public function scopeType($query, $type)
    {
        return $query->where('type', '=', $type);
    }

    public function scopeLimit($query, $limit)
    {
        return $query->take($limit);
    }

    public function scopePostOnly($query)
    {
        return $this->scopeType($query, 'post');
    }

    public function scopePageOnly($query)
    {
        return $this->scopeType($query, 'page');
    }

    public function scopeDescending($query)
    {
        return $query->orderBy('created_at', 'desc')->orderBy('id','desc');
    }

}

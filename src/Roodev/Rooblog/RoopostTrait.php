<?php namespace Roodev\Rooblog;

trait RoopostTrait {

    /**
     * Inisialisasi Model Post
     * 
     * @return Models\Post
     */
    public function post()
    {
        return $this->model->post;
    }

    /**
     * Menyimpan data post ke dalam database
     * setiap proses akan divalidasi terlebih dahulu
     * 
     * @param  array $data Minimal ada title dan content
     * 
     * @return Models\Post|bool Jika sukses mengembalikan object Models\Post
     */
    public function storePost(array $data)
    {
        if ($this->post()->validate($data)) {
            return $this->storePostForce($data);
        } else {
            $this->setErrors($this->post()->errors());

            return false;
        }
    }

    /**
     * Menyimpan data post ke dalam database
     * tanpa proses validasi
     * 
     * @param  array $data Minimal ada title dan content
     * 
     * @return Models\Post
     */
    public function storePostForce(array $data)
    {
        foreach ($data as $key => $value)
            $this->post()->{$key}    = $value;
            $this->post()->save();

            return ($this->post()->id ? $this->post() : 0);
    }

    /**
     * Mengambil banyak data post
     * bisa dibatasi yang digunakan untuk pagination
     * 
     * @param  integer $perPage Berapa banyak data dalam satu halaman
     * 
     * @return Illuminate\Database\Eloquent\Collection
     */
    public function showPosts($perPage = 0)
    {
        return $this->post()->postOnly()->descending()->paginate($perPage);
    }

    /**
     * Mengambil banyak data post dengan limit
     * nilai defaultnya adalah 20
     *
     * @param  integer $limit Limit banyak data yang diambil
     *
     * @return Illuminate\Database\Eloquent\Collection
     */
    public function showPostsWithLimit($limit = 20)
    {
        return $this->post()->postOnly()->descending()->limit($limit)->get();
    }

    /**
     * Mengambil sebuah data post berdasarkan id (primary key)
     * 
     * @param  integer $id Primary key dari data post yang akan diambil
     * 
     * @return Models\Post
     */
    public function showPost($id)
    {
        return $this->post()->pageOnly()->find($id);
    }

    /**
     * Mengambil sebuah data post berdasarkan slug
     *
     * @param  string $slug Slug dari data post yang akan diambil
     *
     * @return Models\Post
     */
    public function showPostBySlug($slug)
    {
        return $this->post()->slug($slug);
    }

    /**
     * Menyimpan pembaharuan dari sebuah post dengan validasi
     *
     * @param  integer $id Id dari data post yang akan diperbaharui
     * @param  array $data Data yang baru
     * @param  boolean $updateSlug Apakah slug akan diperbaharui juga?
     *
     * @return Models\Post|boolean
     */
    public function updatePost($id, array $data, $updateSlug = false)
    {
        if ($this->post()->validate($data)) {
            return $this->updatePostForce($id, $data, $updateSlug);
        } else {
            $this->setErrors($this->post()->errors());
            
            return false;
        }
    }

    /**
     * Menyimpan pembaharuan dari sebuah post tanpa validasi
     *
     * @param  integer $id Id dari sebuah post yang akan diperbaharui
     * @param  array $data Data yang baru
     * @param  boolean $updateSlug Apakah slug akan diperbaharui juga?
     *
     * @return Models\Post|boolean
     */
    public function updatePostForce($id, array $data, $updateSlug = false)
    {
        $this->post           = $this->showPost($id);

        foreach ($data as $key => $value)
            $this->post->{$key} = $value;

        if ($updateSlug)
            $this->post->resluggify();

        $this->post->save();

        return $this->post;
    }

    /**
     * Menghapus sebuah data post dari database
     *
     * @param  integer $id Id dari sebuah post yang akan dihapus
     *
     * @return boolean
     */
    public function destroyPost($id)
    {
        if($post = $this->showPost($id)) {
            return $post->delete();
        } else {
            return false;
        }
    }

}
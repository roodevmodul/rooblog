<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRooPostTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('roo_post', function(Blueprint $t)
        {
            $t->increments('id');

            $t->string('title');
            $t->string('slug')->unique();
            $t->longText('content');
            $t->integer('user_id');
            $t->string('type', 10)->default('post');
            $t->string('status', 10)->default('publish');

            $t->timestamps();
            $t->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('roo_post');
    }

}

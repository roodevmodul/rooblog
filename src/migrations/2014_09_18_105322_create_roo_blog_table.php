<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRooBlogTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('roo_blog', function(Blueprint $t)
        {
            $t->increments('id');

            $t->string('name');
            $t->string('url');
            $t->string('slogan');
            $t->text('description');
            $t->string('status', 10);
            
            $t->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('roo_blog');
    }

}

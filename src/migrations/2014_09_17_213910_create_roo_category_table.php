<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRooCategoryTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('roo_category', function(Blueprint $t)
        {
            $t->increments('id');
            
            $t->integer('parent_id')->default(0);
            $t->string('name');
            $t->string('slug')->unique();
            $t->boolean('removable')->default(1);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('roo_category');
    }

}
